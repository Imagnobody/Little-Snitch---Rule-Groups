[Originaly on github](https://github.com/naveednajam/Little-Snitch---Rule-Groups) by naveednajam  
Forked by me (ImaginaryCS) to update and also switch to the  
[basic Unified hosts version](https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts)  
instead of the  
[Unified hosts + **fakenews** + **gambling** + **porn** version](https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts)

## Todos
- Change **stevenblack_unified_hosts_V1.1.py** to Python 3 code,
since I'm currently focusing on learning Python 3.

## Original README Content
## Little-Snitch---Rule-Groups
This repo provide rule groups for Little Snitch based on unified host list to block ads, malware, fake news and porn

Python script will convert the unified host files provided and maintained by the great team and members at https://github.com/StevenBlack/hosts to the .lsrules files which can be directly subscribed in Little Snitch V4.1
as of now each rule group can hold maximum of 10,000 (rules, domain, hosts) script split the unified host file multiple small files each contains maximum of 10,0000 rules to comply with Little Snitch limitation.
